import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool click = true;

  double value = 0;

  double number() {
    if (click == false) {
      setState(() => value = 1);
    } else {
      setState(() => value = 0);
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          AnimatedContainer(
            duration: Duration(seconds: 1),
            width: double.infinity,
            height: click ? 20 : 300,
            decoration: BoxDecoration(
              color: click ? Colors.green[300] : Colors.cyan,
              boxShadow: [BoxShadow(blurRadius: 5, color: click ? Colors.white : Colors.black)],
            ),
            alignment: Alignment.center,
            onEnd: number,
            child: AnimatedOpacity(
              duration: Duration(
                seconds: 1,
              ),
              opacity: click ? 0: value,
              child: AnimatedDefaultTextStyle(
                style: TextStyle(
                  fontSize: click ? 30 : 70,
                  color: Colors.black,
                ),
                duration: Duration(seconds: 1),
                child: Text(
                  'Text',
                ),
              ),
            ),
          ),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(140, 600, 140, 100),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      click = !click;
                    });
                  },
                  child: AnimatedContainer(
                    duration: Duration(seconds: 1),
                    width: 150,
                    height: 50,
                    decoration: BoxDecoration(
                      color: click ? Colors.green[300] : Colors.cyan,
                      boxShadow: [BoxShadow(blurRadius: 5, color: click ? Colors.white : Colors.black)],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
